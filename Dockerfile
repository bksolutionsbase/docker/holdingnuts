# Select base image: Latest Ubuntu Image with application at repository
FROM docker.io/ubuntu:18.04

LABEL name="briezh/holdingnuts" \
      vendor="BKSolutions" \
      summary="HoldingNuts is an open source multi-platform poker client and server." \
      description="You can play the popular Texas Hold'em variant with people all over the world, meet your friends, run your own games and even setup your own poker-network."

# Install holdingnuts_server from repository
RUN apt-get -y update && apt-get install -y holdingnuts-server && apt-get clean && rm -rf /var/lib/apt/lists/*

LABEL version="latest"

# Create configuration folder and add default setup 
RUN mkdir -p /root/.holdingnuts
COPY ./HoldingnutsServerCfg /root/.holdingnuts/server.cfg

LABEL release="1"

# Define server port
EXPOSE 40888
# Define server volume
VOLUME /root/.holdingnuts

# Start holdingnuts_server as entrypoint
ENTRYPOINT "/usr/games/holdingnuts-server"
